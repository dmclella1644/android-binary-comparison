package com.example.david.binaryfilecompare;

import android.content.Context;
import android.os.AsyncTask;
import android.util.JsonReader;
import android.util.Log;
import android.view.View;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

public class SendServer extends AsyncTask<String, Void, String> {
    public SendServer(Context ctx){
        Context context=ctx;
    }

    // result is the output from the server
    private static String result = "";
    public static String getResult() {
        return result;
    }

    @Override
    protected String doInBackground(String... params) {
        String myurl = "http://10.0.2.2:8080/";
        try {
            /*
             * Since connecting with my machine is more private and does not require a user
             * to add data to the database, the server is located on localhost port 8080.
             * No cloud server was set up, though I could have used Mongodb Atlas instead of
             * putting the server on my computer.
             * The server takes 2 file names, looks for them in the database, compares them,
             * and returns a JSON object with an offset and the next 16 bytes after the offset.
             * The offset is the number of bytes from the beginning to the first difference in
             * the file.
             */
            URL url = new URL(myurl);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            OutputStream outputStream = httpURLConnection.getOutputStream();
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
            String post_data = URLEncoder.encode(params[0] + "/" + params[1], "UTF-8");
            bufferedWriter.write(post_data);
            bufferedWriter.flush();
            bufferedWriter.close();
            outputStream.close();
            InputStream inputStream = httpURLConnection.getInputStream();
            JsonReader reader = new JsonReader(new InputStreamReader(inputStream, "iso-8859-1"));
            int offset = -1;
            String file1 = "";
            String file2 = "";
            String message = "";
            int endTime = -1;
            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();
                if (name.equals("offset")) {
                    offset = reader.nextInt();
                } else if (name.equals("file1")) {
                    file1 = reader.nextString();
                } else if (name.equals("file2")) {
                    file2 = reader.nextString();
                } else if (name.equals("message")){
                    message = reader.nextString();
                } else if (name.equals("time")){
                    endTime = reader.nextInt();
                } else {
                    reader.skipValue();
                }
            }
            reader.endObject();
            inputStream.close();
            httpURLConnection.disconnect();
            //output data. message could be a file not found or both files are the same.
            if (message == ""){
                Log.d("RESULT", "offset: "+Integer.toString(offset)+", file1: " + file1 + ", file2: " + file2 + ", time:" + endTime);
                result = "offset: "+Integer.toString(offset)+", file1: " + file1 + ", file2: " + file2 + ", time:" + endTime;
                return "";
            } else {
                Log.d("RESULT", message + ", time: " + endTime);
                result = message + ", time: " + endTime;
                return "";
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return "error";
        } catch (IOException e) {
            e.printStackTrace();
            return "error";
        }
    }
    @Override
    protected void onPreExecute() {
    }
    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }
}

