package com.example.david.binaryfilecompare;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
// import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private static String fileName1;
    private static String fileName2;
    /*
     * Compare 2 binary files where the file names are
     * inputed in an editText box. These files are read
     * and compared using a local web server (Mongoose).
     * Stop comparing once a difference is found.
     * Do timing by self
     * Program prints offset of first difference and the
     * first 16 bytes from where they differ.
     */
    Context ctx = this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final EditText input = (EditText) findViewById(R.id.file1);
        final EditText input2 = (EditText) findViewById(R.id.file2);
        //final TextView output = (TextView) findViewById(R.id.output);
        Button acquire = (Button) findViewById(R.id.compare);
        acquire.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                fileName1 = input.getText().toString();
                fileName2 = input2.getText().toString();
                SendServer sendServer = new SendServer(ctx);
                sendServer.execute(fileName1, fileName2);
                //send to database and compare
                // output.setText(result);
            }
        });
    }
}
