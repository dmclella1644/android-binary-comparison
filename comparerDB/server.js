var express = require('express'),
    app = express(),
    port = Number(process.env.PORT || 8080);
	
var bodyParser = require('body-parser');
var fs = require('fs');
var mongoose = require('mongoose');
// var path = require('path');
// var request = require('request');
var Stopwatch = require('node-stopwatch').Stopwatch;
var MongoClient = require('mongodb').MongoClient;
// var jsdiff = require('diff');

//connecting server to webpage
mongoose.connect('mongodb://localhost:27017/comparerDB', {
    useNewUrlParser: true
});

// const FileComparison = require('./mongoose/mongoose');
// const BinaryFile = require('./mongoose/mongoose');
const AFile = require('./mongoose/mongoose');
// const BFile = require('./mongoose/mongoose');

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

//convert array buffer to buffer
function toBuffer(ab) {
    var buf = new Buffer(ab.byteLength);
    var view = new Uint8Array(ab);
    for (var i = 0; i < buf.length; ++i) {
        buf[i] = view[i];
    }
    return buf;
}

// find min of two ints
function min(a, b) {
	if (a < b){
		return a;
	} else {
		return b;
	}
}

app.get('/', function(req, res) {
	// console.log("adding file3.bin to database");
	// var data = {
		// file: fs.readFileSync('file3.bin'),
		// name: "file3.bin"
	// };
	// request.post({url: 'http://127.0.0.1:8080/binaryFiles', json: data});
	// fs.close();
	// const id = "5b6e2d60bd97203f4c231e93";
	// AFile.remove({_id: id}).exec()
	// .then(result => {
		// res.status(200).json(result);
	// })
	// .catch(err => {
		// console.log(err);
		// res.status(500).json({
			// error: err
		// });
	// });
	// AFile.find().exec()
	// .then(files => {
		// console.log(files);
		// if (files.length > 0) {
			// res.status(200).json(files);
		// } else {
			// res.status(404).json({
				// message: "No entries found"
			// });
		// }
	// })
	// .catch(err => {
		// console.log(err);
		// res.status(500).json({error: err});
	// });
});

app.post('/binaryFiles', (req, res, next) => {
	// console.log(req.body.file);
	// console.log(req.body.name);
	if(req.body.file == undefined){
	  res.redirect('/?msg=2');
	}else{
		 
		/**
		 * Create new record in mongoDB
		 */
		// var fullPath = "binaryFiles/"+req.body.name;
		// console.log(req.body.file);
		const binFile = new AFile({
			_id: new mongoose.Types.ObjectId(),
			file: req.body.file,
			file_name: req.body.name
		});
		// console.log(JSON.stringify(req.body.file));
		console.log(binFile);
	  binFile.save().then(result => {
        // console.log(result);
		res.status(201).json({
            message: "Getting requests from /mongoose",
			file: result
		});
	 }).catch(err => {
        console.log(err);
		res.status(500).json({
            error: err
        });
    });
  }
});

app.post('/', (req, res, next) => {
	/*
	 * This first part could have been handled a bit better.
	 * Instead of saying, "Oh, Here is the file names!" the
	 * code looks for the file names. In req.body, the file
	 * names are between "" and separated by a /. So, it
	 * looks like "file1.bin/file2.bin"
	 * A better method would have been to put the file names
	 * in a JSON object then parse the object.
	 */
	var file_names = JSON.stringify(req.body);
	var temp = file_names.split("\"");
	var files = temp[1].split("/");
	var lines1;
	var lines2;
	// reading the entire file and checking if it exists
	try {
		lines1 = fs.readFileSync(files[0], 'utf-8');
	} catch (err) {
		if (err.code === 'ENOENT') {
			res.status(500).json({
				message: "file 1 was not found"
			});
		} else {
			res.status(500).json({
				message: "err: " + err + " has occured"
			});
		}
		
	}
	try {
		lines2 = fs.readFileSync(files[1], 'utf-8');
	} catch (err) {
		if (err.code === 'ENOENT') {
			res.status(500).json({
				message: "file 2 was not found"
			});
		} else {
			res.status(500).json({
				message: "err: " + err + " has occured"
			});
		}
	}
	// checking if files are empty
	if (lines1.length <= 0) {
		if (lines2.length <= 0) {
			res.status(404).json({
				message: "both files are empty"
			});
		} else {
			res.status(404).json({
				message: "file1 is empty"
			});
		}
	} else {
		if (lines2.length <= 0) {
			res.status(404).json({
				message: "file2 is empty"
			});
		} else {
			// comparing files by chucks of Strings. Timer starts
			var stopwatch = Stopwatch.create();
			stopwatch.start();
			var i = 0;
			var j = 0;
			var variance = Math.floor(Math.sqrt(min(lines1.length, lines2.length)));
			while (lines1.substring(i*variance,(i+1)*variance) == lines2.substring(i*variance,(i+1)*variance) && (i+1)*variance-1 < min(lines1.length, lines2.length)) {
				i++;
			}
			while(lines1.substring(i*variance+j,i*variance+j+1) == lines2.substring(i*variance+j,i*variance+j+1) && i*variance+j < min(lines1.length, lines2.length)){
				j++;
			}
			stopwatch.stop();
			// timer ends
			var endTime = stopwatch.elapsedMilliseconds;
			// outputing, probably not needed but just in case
			if (i*variance + j +16 >= min(lines1.length, lines2.length)){
				if (i*variance + j >= min(lines1.length, lines2.length)) {
					if (lines1.length == lines2.length) {
						res.status(200).json({
							message: "both files are the same",
							time: endTime
						});
					} else {
						if (lines1.length > lines2.length) {
							if (i*variance + j +16 >= lines1.length) {
								// console.log("offset: " + i*variance + j + ", file1: " + lines1.substring(i*variance + j) + ", file2: ");
								res.status(201).json({
									offset: i*variance + j,
									file1: lines1.substring(i*variance + j),
									time: endTime
								});
							} else {
								// console.log("offset: " + i*variance + j + ", file1: " + lines1.substring(i*variance + j, i*variance + j+16) + ", file2: ");
								res.status(201).json({
									offset: i*variance + j,
									file1: lines1.substring(i*variance + j, i*variance + j+16),
									time: endTime
								});
							}
						} else {
							if (i*variance + j +16 >= lines2.length) {
								// console.log("offset: " + i*variance + j + ", file1: , file2: " + lines2.substring(i*variance + j));
								res.status(201).json({
									offset: i*variance + j,
									file2: lines2.substring(i*variance + j),
									time: endTime
								});
							} else {
								// console.log("offset: " + i*variance + j + ", file1: , file2: "+ lines1.substring(i*variance + j, i*variance + j+16));
								res.status(201).json({
									offset: i*variance + j,
									file2: lines2.substring(i*variance + j, i*variance + j+16),
									time: endTime
								});
							}
						}
					}
				} else {
					if (lines1.length > lines2.length) {
						if (i*variance + j +16 >= lines1.length) {
							// console.log("offset: " + i*variance + j + ", file1: " + lines1.substring(i*variance + j) + ", file2: ");
							res.status(201).json({
								offset: i*variance + j,
								file1: lines1.substring(i*variance + j),
								file2: lines2.substring(i*variance + j),
								time: endTime
							});
						} else {
							// console.log("offset: " + i*variance + j + ", file1: " + lines1.substring(i*variance + j, i*variance + j+16) + ", file2: ");
							res.status(201).json({
								offset: i*variance + j,
								file1: lines1.substring(i*variance + j, i*variance + j+16),
								file2: lines2.substring(i*variance + j),
								time: endTime
							});
						}
					} else if (lines1.length < lines2.length) {
						if (i*variance + j +16 >= lines2.length) {
							// console.log("offset: " + i*variance + j + ", file1: " + lines1.substring(i*variance + j) + ", file2: ");
							res.status(201).json({
								offset: i*variance + j,
								file1: lines1.substring(i*variance + j),
								file2: lines2.substring(i*variance + j),
								time: endTime
							});
						} else {
							// console.log("offset: " + i*variance + j + ", file1: " + lines1.substring(i*variance + j, i*variance + j+16) + ", file2: ");
							res.status(201).json({
								offset: i*variance + j,
								file1: lines1.substring(i*variance + j),
								file2: lines2.substring(i*variance + j, i*variance + j+16),
								time: endTime
							});
						}
					} else {
						// console.log("offset: " + i*variance + j + ", file1: " + lines1.substring(i*variance + j) + ", file2: " + lines2.substring(i*variance + j));
						res.status(201).json({
							offset: i*variance + j,
							file1: lines1.substring(i*variance + j),
							file2: lines2.substring(i*variance + j),
							time: endTime
						});
					}
				}
			} else {
				// console.log("offset: " + i*variance + j + ", file1: " + lines1.substring(i*variance + j, i*variance + j+16) + ", file2: " + lines2.substring(i*variance + j, i*variance + j+16));
				res.status(201).json({
					offset: i*variance + j,
					file1: lines1.substring(i*variance + j, i*variance + j+16),
					file2: lines2.substring(i*variance + j, i*variance + j+16),
					time: endTime
				});
			}
		}
	}
});

app.get("/:mongooseId", (req, res, next) => {
    const id = req.params.mongooseId;
    FileComparison.findById(id).exec()
      .then(doc => {
        console.log(doc);
		if (doc) {
			res.status(200).json(doc);
		} else {
			res.status(404).json({message: "No valid entry exists for given ID"});
		}
	
      })
      .catch(err => {
        console.log(err);
		res.status(500).json({error: err});
      });
});

app.delete("/:mongooseId", (req, res, next) => {
	const id = req.params.mongooseId;
	FileComparison.remove({_id: id}).exec()
	.then(result => {
		res.status(200).json(result);
	})
	.catch(err => {
		console.log(err);
		res.status(500).json({
			error: err
		});
	});
});

app.post('/fileupload', function (req, res) {
	
    // var form = new formidable.IncomingForm();
    // form.uploadDir = __dirname + "/data";
    // form.keepExtensions = true;
    // form.parse(req, function(err, fields, files) {
        // if (!err) {
          // console.log('File uploaded : ' + files.file.path);
          // grid.mongo = mongoose.mongo;
          // var conn = mongoose.createConnection('..mongo connection string..');
          // conn.once('open', function () {
          // var gfs = grid(conn.db);
          // var writestream = gfs.createWriteStream({
              // filename: files.file.name
          // });
          // fs.createReadStream(files.file.path).pipe(writestream);
       // });
     // }        
   // });
   // form.on('end', function() {        
       // res.send('Completed ..... go and check fs.files & fs.chunks in  mongodb');
   // });

});

app.listen(port, function() {
    console.log('Listening on port ' + port);
});
