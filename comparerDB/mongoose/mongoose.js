const mongoose = require('mongoose');
// var path = require('path');

// var uploads_base = path.join(__dirname, "uploads");
// var uploads = path.join(uploads_base, "u");

// const fileSchema = mongoose.Schema({
    // _id: mongoose.Schema.Types.ObjectId,
    // file_path: String,
	// file_path2: String
// });
// const BinaryFileSchema = new mongoose.Schema({
  // _id: mongoose.Schema.Types.ObjectId,
  // binary: Buffer
// });
const File = new mongoose.Schema({
	_id: mongoose.Schema.Types.ObjectId,
	file: {data: Buffer, contentType: String},
	file_name: String
});
// const File2 = new mongoose.Schema({
	// _id: mongoose.Schema.Types.ObjectId,
	// data: [Buffer],
	// file_name: String
// });

module.exports = mongoose.model("AFile", File);
// module.exports = mongoose.model("BFile", File2);
// module.exports = mongoose.model("BinaryFile", BinaryFileSchema);
// module.exports = mongoose.model('FileComparison', fileSchema);
